# 도커를 활용한 mysql 설치하기

1. Docker를 설치한다.(윈도우) [[다운로드]](https://download.docker.com/win/stable/Docker%20Desktop%20Installer.exe)
<br>

2. 이 레포를 클론한다.
`git clone <repo url>`
<br>

3. 클론한 프로젝트 루트 디렉토리로 이동한다.
<br>

4. 도커 컴포즈로 실행한다.
`docker-compose up` 또는 `docker-compose up -d`
<br>

5. 종료시 다른 cmd창을 열어 아래의 명령어를 입력한다.
`docker-compose down` 또는 `Ctrl + C`

